/**
  ************************************************************************************************
  * @file	 Modbus.h
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    15-December-2016
  * @brief   Header for HD44780 Display.
  *************************************************************************************************
  */
#ifndef __Modbus
#define __Modbus

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"

// -------------------------------------------------------- Exported define ----------------------:
#define MODBUS_DEVICE_ADDRESS					101

#define MODBUS_DISCRETE_IPUTS_NUM				0
#define MODBUS_COILS_NUM 						0
#define MODBUS_INPUT_REGISTERS_NUM				20			// TI!
#define MODBUS_HOLDING_REGISTERS_NUM 			0

#define EXEPTION_CODE_01		0x01
#define EXEPTION_CODE_02		0x02
#define EXEPTION_CODE_03		0x03
#define EXEPTION_CODE_04		0x04
#define EXEPTION_CODE_05		0x05

#define MODBUS_ERROR_CODE		0x80

// -------------------------------------------------------- Exported types -----------------------:
// Structure of Input and Holding Modbus register's Tags =============:
typedef struct
{
	uint16_t *Value;  	// Pointer to item's Value.
}
tUint16Tag;

// Structure of Modbus Coils and Discrete Inputs Tags ================:
typedef struct
{
	_Bool *Value;  	// Pointer to item's Value.
}
tBoolTag;

typedef enum
{
	READ_COILS	= 0x01,
	READ_DISCRETE_INPUTS,
	READ_HOLDING_REGISTERS,
	READ_INPUT_REGISTER,
	WRITE_SINGLE_COIL,
	WRITE_SINGLE_REGISTER,
	READ_EXCEPTION_STATUS,
	DIAGNOSTIC
}
tModbusFuncCode;
// -------------------------------------------------------- Private macros -----------------------:

// -------------------------------------------------------- Exported variables -------------------:

// -------------------------------------------------------- Exported functions -------------------:
extern void Modbus_Init(void);
extern void ModbusProcess(uint8_t *request_buff, uint8_t *response_buff, uint16_t *response_size);
extern void CreateModbusInputRegTag16(uint16_t *source, uint16_t index);
extern uint16_t ModbusCRC(uint8_t *buf, uint16_t len);		 // Compute the MODBUS RTU CRC.


// -------------------------------------------------------- Exported constants -------------------:


#endif /* __Modbus */

/**********************************************************************************END OF FILE****/
