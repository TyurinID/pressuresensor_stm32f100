/**
  *************************************************************************************************
  * @file	 Modbus.c
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    15-December-2016
  * @brief   Library for Modbus protocol. Generate Modbus registers.
  * 			Modbus request and response functions definitions.
  *************************************************************************************************
  */

// -------------------------------------------------------- Includes -----------------------------:
#include "Modbus/Modbus.h"


// --------------------------------------------------------- Global variables --------------------:

// Modbus registers declaration ============================================:
	//static tBoolTag	ModbusDiscreteInut[MODBUS_DISCRETE_IPUTS_NUM];
	//static tBoolTag	ModbusCoil[MODBUS_COILS_NUM];
static tUint16Tag	ModbusInutRegister[MODBUS_INPUT_REGISTERS_NUM];
	//static tUint16Tag	ModbusHoldingRegister[MODBUS_HOLDING_REGISTERS_NUM];


// --------------------------------------------------------- Private variables -------------------:
static uint16_t dummyValue = 0xffff;

// --------------------------------------------------------- Private function prototypes ---------:
static uint16_t getValue16(tUint16Tag modbus_register);


// --------------------------------------------------------- Global functions --------------------:
/**********************************
  *	&	@brief  Initialize modbus registers by dummy values.
  * & 	@param  None
  * & 	@retval None
  *********************************/
void Modbus_Init()
{
	for(uint16_t i=0; i<MODBUS_INPUT_REGISTERS_NUM; i++)
		ModbusInutRegister[i].Value = &dummyValue;
}


/**********************************@ModbusProcess
  *	&	@brief  General Modbus Function.
  *				Parse message, execute command and generate response.
  * & 	@param  None
  * & 	@retval None
  *********************************/
void ModbusProcess(uint8_t *request_buff, uint8_t *response_buff, uint16_t *response_size)
{
	uint16_t startAddr  = 0;
	uint16_t quantity	= 1;
	switch ((tModbusFuncCode)request_buff[0])
	{
		case READ_INPUT_REGISTER:
			startAddr = ((uint16_t)request_buff[1])<<8 || (uint16_t)request_buff[2];
			quantity = ((uint16_t)request_buff[3])<<8 || (uint16_t)request_buff[4];
			// Check for errors =============:
			if( 1 <= quantity && quantity < MODBUS_INPUT_REGISTERS_NUM )
			{
				if(1 <= startAddr && (startAddr+quantity) < MODBUS_INPUT_REGISTERS_NUM)
				{
					// Form request =========:
					uint16_t byteCnt = 0;
					byteCnt = quantity*2;
					response_buff[0] = READ_INPUT_REGISTER;
					response_buff[1] = byteCnt;
					for(uint16_t i=0; i<quantity; i++)
					{
						response_buff[2+i*2] = (uint8_t)(getValue16(ModbusInutRegister[startAddr+i])>>8);
						response_buff[3+i*2] = (uint8_t)getValue16(ModbusInutRegister[startAddr+i]);
					}
					*response_size = 2 + quantity*2;
				}
				else	// error:
				{
					response_buff[0] = MODBUS_ERROR_CODE + READ_INPUT_REGISTER;
					response_buff[1] = EXEPTION_CODE_02;
					*response_size = 2;
				}
			}
			else	// error:
			{
				response_buff[0] = MODBUS_ERROR_CODE + READ_INPUT_REGISTER;
				response_buff[1] = EXEPTION_CODE_03;
				*response_size = 2;
			}
			break;
		default:
			response_buff[0] = MODBUS_ERROR_CODE + READ_INPUT_REGISTER;
			response_buff[1] = EXEPTION_CODE_01;
			*response_size = 2;
			break;
	}
}


/**********************************
  *	&	@brief  Add new connection between modbus register
  *				and RAM value.
  * & 	@param  *source - pointer to value in RAM;
  * 			index   - number of register;
  * & 	@retval None
  *********************************/
void CreateModbusInputRegTag16(uint16_t *source, uint16_t index)
{
	ModbusInutRegister[index].Value = source;
}


/**********************************
  *	&	@brief  Compute the MODBUS RTU CRC.
  * & 	@param  *buf - pointer to message;
  * 			len  - length of message;
  * & 	@retval 16bit crc code.
  *********************************/
uint16_t ModbusCRC(uint8_t *buf, uint16_t len)
{
	uint16_t crc = 0xFFFF;

  for (uint16_t pos = 0; pos < len; pos++) {
    crc ^= (uint16_t)buf[pos];          // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
    }
  }
  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
  return crc;
}


// --------------------------------------------------------- Private functions -------------------:
/***********************
 *		@brief  Return value saved in Modbus Register.
 *  	@param  Modbus Register.
 *  	@retval Value.
 **********************/
static uint16_t getValue16(tUint16Tag modbus_register)
{
	return (*(modbus_register.Value));
}





/**********************************************************************************END OF FILE****/
