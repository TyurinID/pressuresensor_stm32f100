/**
  ************************************************************************************************
  * @file	 uart.c
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    28-December-2016
  * @brief   Functions for uart conversation.
  * 		 UART interrupt functions.
  *************************************************************************************************
  */
// -------------------------------------------------------- Includes -----------------------------:
#include "Modbus/uart.h"
#include <stdlib.h>
#include "Modbus/Modbus.h"

// -------------------------------------------------------- Private macros -----------------------:
//#define START_RECEIVE()
#define TX_ENABLE() 		HAL_GPIO_WritePin( UART_DE_PORT, UART_DE_PIN, 1 )
#define RX_ENABLE()   		HAL_GPIO_WritePin( UART_DE_PORT, UART_DE_PIN, 0 )
#define	DE_CLK_ENABLE()		__GPIOC_CLK_ENABLE();
// --------------------------------------------------------- Global variables --------------------:

// --------------------------------------------------------- Private variables -------------------:
static UART_HandleTypeDef UartHandle;
static tUart 		gUart;			// Main structure for Uart communication.
static uint16_t 	start_time = 0;
static uint16_t 	tx_size = 0;		// Number of bytes to be sent.

// --------------------------------------------------------- Private function prototypes ---------:
static bool parseMessage(uint8_t *rx_buff, uint16_t rx_size, uint8_t *tx_buff, uint16_t *tx_size);


// --------------------------------------------------------- Global functions --------------------:

 /***********************
  *		@brief  Configure uart settings.
  *				Initialize uart structure and other parameters.
  *  	@param  None
  *  	@retval "1" in case of success, "0" if any error.
  **********************/
uint8_t UartInit(void)
{
	// Configure DE_PIN output =================:
	GPIO_InitTypeDef GPIO_InitStruct;
	DE_CLK_ENABLE();
	GPIO_InitStruct.Pin = 	UART_DE_PIN;
	GPIO_InitStruct.Mode = 	GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = 	GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(UART_DE_PORT, &GPIO_InitStruct);

	// Standard 8N1 ==:
	UartHandle.Instance        = USARTx;
	UartHandle.Init.BaudRate   = 9600;
	UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	UartHandle.Init.StopBits   = UART_STOPBITS_1;
	UartHandle.Init.Parity     = UART_PARITY_NONE;
	UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UartHandle.Init.Mode       = UART_MODE_TX_RX;

	if(HAL_UART_DeInit(&UartHandle) != HAL_OK)
			return 0;

	if(HAL_UART_Init(&UartHandle) != HAL_OK)
			return 0;
	

	gUart.Stage = RECEIVE;
	gUart.Timeout = 2000;  // ms
	gUart.TxReady = RESET;
	return 1;
}

/***********************
 *		@brief  Function must be executed cyclically.
 *				Receive msg, parse it. Form answer and send it.
 *  	@param  None
 *  	@retval None
 **********************/
void UartProcess(uint16_t period)
{
	switch(gUart.Stage)				// UART works stage by stage.
	{
		// Wait for Rx message ======================================================================:
		case RECEIVE:
			RX_ENABLE();
			if(HAL_UART_Receive_DMA(&UartHandle, gUart.RxBuff, RX_MESSAGE_MAX_SIZE) != HAL_OK)
			{
				gError = UART_RX_ERR;
				gUart.Stage = NONE;			// TI!
			}
			break;

		// Wait for Rx message ======================================================================:
		case WAIT_RX:
			if(__HAL_UART_GET_FLAG(&UartHandle, UART_FLAG_IDLE))		// check idle state
			{
				uint16_t rx_size = RX_MESSAGE_MAX_SIZE - UartHandle.hdmarx->Instance->CNDTR;

				if(HAL_DMA_Abort(UartHandle.hdmarx) != HAL_OK)
				{
					gError = UART_DMA_ERR;
					gUart.Stage = NONE;			// TI!
				}

				// If msg received, parse it and form answer ===:
				if (parseMessage(gUart.RxBuff, rx_size, gUart.TxBuff, &tx_size) == 0)
					gUart.Stage = RECEIVE;		// if not device addr or another error.
				gUart.Stage = SEND;
			}
			break;

		// Sending message ========================================================================:
		case SEND:
			TX_ENABLE();
			gUart.TxReady = RESET;
			if(HAL_UART_Transmit_DMA(&UartHandle, gUart.TxBuff, tx_size) != HAL_OK)			// Start the transmission process.
			{
				gError = UART_TX_ERR;
				gUart.Stage = RECEIVE;
			}
			else
			{
				gUart.Stage = WAIT_TX;
				start_time = 0;
			}
			break;

		// Wait for end of message transfer ========================================================:
		case WAIT_TX:
			if(gUart.TxReady != SET)
			{
				if( ++start_time > (gUart.Timeout/period))
				{
					gError = UART_TIMEOUT_ERR;
					gUart.Stage = RECEIVE;
				}
			}
			else
				gUart.Stage = RECEIVE;
			break;

		// Wait for new commands ====================================================================:
		case NONE:
		default:
			__NOP();
			break;
	}
}

// --------------------------------------------------------- Private functions -------------------:
/***********************
 *		@brief  Check device address and CRC.
 *				Extract modbus PDU and parse it.
 *				Form request.
 *  	@param  None
 *  	@retval "1" if msg parsed, "0" if not device addr or another error.
 **********************/
static bool parseMessage(uint8_t *rx_buff, uint16_t rx_size, uint8_t *tx_buff, uint16_t *tx_size)
{
	// Work with ADU ===============================:
	if(rx_buff[0] != MODBUS_DEVICE_ADDRESS)
		return 0;
	uint16_t crc;
	crc = ModbusCRC(rx_buff, rx_size-2);
	if( rx_buff[rx_size-2] != (uint8_t)crc  ||  rx_buff[rx_size-1] != (uint8_t)(crc>>8) )
		return 0;

	// Read modbus request (PDU) and form answer =========:
	ModbusProcess(rx_buff+1, tx_buff+1, tx_size);

	// Form ADU ====================================:
	tx_buff[0] = MODBUS_DEVICE_ADDRESS;
	*tx_size += 1;

	crc = ModbusCRC(tx_buff, *tx_size);
	tx_buff[(*tx_size)++] = (uint8_t)crc;
	tx_buff[(*tx_size)++] = (uint8_t)(crc>>8);
	return 1;
}

//static uint8_t crc8d5(uint8_t *pcBlock, unsigned int len, uint8_t *crc)
//{
//	unsigned int i;
//
//  while (len--)
//  {
//    *crc ^= *pcBlock++;
//
//    for (i = 0; i < 8; i++)
//        *crc = *crc & 0x80 ? (*crc << 1) ^ 0xd5 : *crc << 1;
//  }
//  return *crc;
//}

// --------------------------------------------------------- Private constants -------------------:

// ######################################################### Interrupt functions ##################:

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	gUart.TxReady = SET;
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	return;
}

///**
//  * @brief  UART error callbacks
//  * @param  UartHandle: UART handle
//  * @note   This example shows a simple way to report transfer error, and you can
//  *         add your own implementation.
//  * @retval None
//  */
////void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
////{
//    //Error_Handler();
////}




/**
  * @brief  This function handles DMA interrupt request.  
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data transmission     
  */
void USARTx_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(UartHandle.hdmarx);
}

/**
  * @brief  This function handles DMA interrupt request.
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data reception    
  */
void USARTx_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(UartHandle.hdmatx);
}


/**
  * @brief  This function handles UART interrupt request.  
  * @param  None
  * @retval None
  * @Note   This function is redefined in "main.h" and related to DMA  
  *         used for USART data transmission     
  */
void USARTx_IRQHandler(void)
{
  HAL_UART_IRQHandler(&UartHandle);
}




/**********************************************************************************END OF FILE****/

