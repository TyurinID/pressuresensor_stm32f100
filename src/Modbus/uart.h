/**
  ************************************************************************************************
  * @file	 uart.h
  * @author  Tyurin Ivan
  * @version V1.0
  * @date    28-December-2016
  * @brief   Header for UART. Configure here all pins and uarts.
  *************************************************************************************************
  */
#ifndef UART_H
#define UART_H

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"


// -------------------------------------------------------- Exported define ----------------------:
#define RX_MESSAGE_MAX_SIZE				1024

/* Definition for USARTx clock resources */
#define USARTx                           USART3
#define USARTx_CLK_ENABLE()              __USART3_CLK_ENABLE()
#define DMAx_CLK_ENABLE()                __HAL_RCC_DMA1_CLK_ENABLE()
#define USARTx_RX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __USART3_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __USART3_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_10
#define USARTx_TX_GPIO_PORT              GPIOB
#define USARTx_RX_PIN                    GPIO_PIN_11
#define USARTx_RX_GPIO_PORT              GPIOB

/* Definition for USARTx's DMA */
#define USARTx_TX_DMA_CHANNEL             DMA1_Channel2
#define USARTx_RX_DMA_CHANNEL             DMA1_Channel3

/* Definition for USARTx's NVIC */
#define USARTx_DMA_TX_IRQn                DMA1_Channel2_IRQn
#define USARTx_DMA_RX_IRQn                DMA1_Channel3_IRQn
#define USARTx_DMA_TX_IRQHandler          DMA1_Channel2_IRQHandler
#define USARTx_DMA_RX_IRQHandler          DMA1_Channel3_IRQHandler

/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART3_IRQn
#define USARTx_IRQHandler                USART3_IRQHandler

// Configure DE pin for MAX485 ==================:
#define UART_DE_PORT 		GPIOC
#define UART_DE_PIN  		GPIO_PIN_4

// -------------------------------------------------------- Exported types -----------------------:
// Uart structure ===============:
typedef enum
{
	RECEIVE,					// Configure uart for reception.
	WAIT_RX,					// Wait for end of Rx message.
	SEND,						// Sending message.
	WAIT_TX,					// Wait for end of message transfer. Turn ON Receive process.
	NONE						// Wait for new commands.
}	tUartStage;

typedef struct {
	tUartStage 		Stage;
	uint16_t		Timeout;			// Max time of message transfer.
	uint8_t			TxBuff[32];
	uint8_t			RxBuff[32];
	uint16_t		RxSize;				// Number of received bytes.
	bool			TxReady;
} tUart;

typedef enum
{
	UART_OK 	= 0,		// No errors. 
	TX_ERR	 	= 1,		// 
	RX_ERR		= 2,   	// 
	VERIFY_ERR= 3,    // Wrong message format or crc code. 
	TIMEOUT_ERR=4,		// 
} TUART_Error;



// -------------------------------------------------------- Exported variables -------------------:
//extern tUart gUart;


// -------------------------------------------------------- Exported functions -------------------:
extern uint8_t 	UartInit(void);
extern void 	UartProcess(uint16_t period);

#endif

