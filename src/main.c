/**
  *************************************************************************************************
  * @file	 main.c
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    15-December-2016
  * @brief   Main program body
  *************************************************************************************************
  */

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"
#include <stdlib.h>
#include "ADC/adc.h"
#include "ADC/PressureSensor.h"
#include "Display/HD44780.h"
#include "Modbus/Modbus.h"
#include "Modbus/uart.h"

// --------------------------------------------------------- Private define ----------------------:
#define MAIN_CYCLE_PERIOD					1 		// in ms
#define PRESSURE_CALCULATION_PERIOD			1000	// in ms

#define AUTOZERO_CALIBRATION	// The autozero pressure sensor procedure should be done
								// for the first device's launch.

// -------------------------------------------------------- Private macros -----------------------:
#define isItTime(period)		((current_cycle)% ((period)/MAIN_CYCLE_PERIOD) == 0)


// --------------------------------------------------------- Global variables --------------------:
tError 		gError;
uint16_t	gPressure;		// Calculated pressure in Pa.

// --------------------------------------------------------- Private variables -------------------:

// --------------------------------------------------------- Private function prototypes ---------:
void SystemClock_Config(void);

// --------------------------------------------------------- Global functions --------------------:



/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @main
  *	&	@brief  Main program
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
int main(void)
{
  // STM32F1xx HAL library initialization =========================:
  HAL_Init();

  // Configure the system clock to 24 MHz, Tsys = 41.7ns ==========:
  SystemClock_Config();

  gPressure = 0;

  // Configure TIM3 and ADC peripheral ============================:
  TIM_Config();
  ADC_Config();
  // Run the ADC calibration ======================================:
  if (HAL_ADCEx_Calibration_Start(&AdcHandle) != HAL_OK)
	  gError = ADC_CALIBRATION_ERR;
  // Run timer for ADC ============================================:
  if (HAL_TIM_Base_Start(&TimHandle) != HAL_OK)
	  gError = TIMER_INIT_ERR;

  // Initialize press_sensor parameters by default ================:
  PressSens_Init();

  // Initialize UART3 for work with modus =========================:
  UartInit();

  // Initialize modbus and create modbus register for Pressure ====:
  Modbus_Init();
  CreateModbusInputRegTag16(&gPressure, 0);


  // Initialize LCD Display =======================================:
  HD44780_Init();


  // Run ADC + DMA ================================================:
  if (HAL_ADC_Start_DMA(&AdcHandle, (uint32_t *)gPressureBuff, PRESSURE_BUFF_SIZE) != HAL_OK)
	  gError = ADC_START_ERR;

#ifdef	AUTOZERO_CALIBRATION
  // The autozero pressure sensor procedure should be done for the first device's launch:
  PressSens_Autozero();
#endif

  // todo: check for errors!


  // Infinite loop ================================================:
  static uint32_t current_cycle = 0;
  while (1)
  {
	  if(gError != NO_ERROR)
	  {
		  sprintf(gDisplayingString, "    Error %d     ", gError);
		  while(gError != NO_ERROR)
			  HD44780_displayString(gDisplayingString);
	  }

	  // Calculate current Pressure value =============:
	  if(isItTime(PRESSURE_CALCULATION_PERIOD))
	  {
		  gPressure = PressSens_Calculate();
		  sprintf(gDisplayingString, "    P = %d Pa     ", gPressure);
	  }

	  // Display current Pressure symbol by symbol ====:
	  HD44780_displayString(gDisplayingString);

	  // Modbus process through UART ==================:
	  UartProcess(MAIN_CYCLE_PERIOD);

	  HAL_Delay(MAIN_CYCLE_PERIOD);
	  // todo: period by timer!
  }
}










/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @SystemClock_Config
  * & 	 @brief
  * &   	 	The system Clock is configured as follow :
  * &       	System Clock source            = PLL (HSE)
  * &       	SYSCLK(Hz)                     = 24000000
  * &     		HCLK(Hz)                       = 24000000
  * &       	AHB Prescaler                  = 1
  * &       	APB1 Prescaler                 = 1
  * &       	APB2 Prescaler                 = 1
  * &       	HSE Frequency(Hz)              = 8000000
  * &       	HSE PREDIV1                    = 2
  * &       	PLLMUL                         = 6
  * &       	Flash Latency(WS)              = 0
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};
  
  // Enable HSE Oscillator and activate PLL with HSE as source =====================:
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV2;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
	  while(1); 				// Initialization Error.


  // Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 ======:
  //   clocks dividers ==============================================================:
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_0)!= HAL_OK)
	  while(1); 				// Initialization Error.
}


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @Error_Handler
  * & 	@brief  Error Handler.
  * &		Function is executed in case of any error.
  * &	@param  None
  * &	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void Error_Handler(void)
{
  // LED4 is toggling at a frequency of 1Hz  =========================:
  while(1)
  {
    // Toggle LED4 ======================:
    BSP_LED_Toggle(LED4);
    HAL_Delay(500);
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif


/**********************************************************************************END OF FILE****/
