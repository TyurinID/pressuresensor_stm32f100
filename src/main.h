/**
  ************************************************************************************************
  * @file	 main.h
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    15-December-2016
  * @brief   Main program body
  *************************************************************************************************
  */
#ifndef __MAIN_H
#define __MAIN_H

// #################################################### Includes ##################################:
#include "stm32f1xx_hal.h"
#include "stm32vl_discovery.h"

// ################################################# Exported types ###############################:
typedef enum {
	NO_ERROR,
	ADC_CALIBRATION_ERR,
	ADC_DMA_ERR,
	ADC_START_ERR,
	AUTOZERO_TIMEOUT_ERR,
	TIMER_INIT_ERR,
	UART_RX_ERR,
	UART_DMA_ERR,
	UART_TIMEOUT_ERR,
	UART_TX_ERR

}	tError;

// ################################################# Exported define ##############################:
#define bool _Bool								// Boolean type.
// ################################################# Exported macro ###############################:

// ################################################# Exported variables ###########################:
extern tError 	gError;

// ################################################# Exported functions ###########################:
extern void Error_Handler(void);

// ################################################# Exported constants ###########################:


#endif /* __MAIN_H */

/**********************************************************************************END OF FILE****/
