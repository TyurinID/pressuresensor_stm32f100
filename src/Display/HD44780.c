/**
  *************************************************************************************************
  * @file	 HD44780.c
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    25-December-2016
  * @brief   Library for HD44780 display. Initialization function.
  * 			(++) Configure initPins() function according to your pinout.
  *
  *************************************************************************************************
  */

// -------------------------------------------------------- Includes -----------------------------:
#include "Display/HD44780.h"

// --------------------------------------------------------- Global variables --------------------:
char gDisplayingString[DISPLAING_STRING_SIZE] = "";

// -------------------------------------------------------- Private macros -----------------------:



 /***********************@HD44780_SET_DATA
  *		@brief  Macro set 8bit from DataCode to GPIOs,
  *				defined for HD44780 data pins.
  *  	@param  uint8_t DataCode.
  *  	@retval None
  **********************/
#define HD44780_SET_DATA(DataCode)	\
		HAL_GPIO_WritePin( HD44780_D0_PORT, HD44780_D0_PIN, ((DataCode & (1<<0))>>0)  );\
		HAL_GPIO_WritePin( HD44780_D1_PORT, HD44780_D1_PIN, ((DataCode & (1<<1))>>1)  );\
		HAL_GPIO_WritePin( HD44780_D2_PORT, HD44780_D2_PIN, ((DataCode & (1<<2))>>2)  );\
		HAL_GPIO_WritePin( HD44780_D3_PORT, HD44780_D3_PIN, ((DataCode & (1<<3))>>3)  );\
		HAL_GPIO_WritePin( HD44780_D4_PORT, HD44780_D4_PIN, ((DataCode & (1<<4))>>4)  );\
		HAL_GPIO_WritePin( HD44780_D5_PORT, HD44780_D5_PIN, ((DataCode & (1<<5))>>5)  );\
		HAL_GPIO_WritePin( HD44780_D6_PORT, HD44780_D6_PIN, ((DataCode & (1<<6))>>6)  );\
		HAL_GPIO_WritePin( HD44780_D7_PORT, HD44780_D7_PIN, ((DataCode & (1<<7))>>7)  )

// Set DE pin for HD44780 display =====================:
#define HD44780_SET_DE()		HAL_GPIO_WritePin( HD44780_DE_PORT, HD44780_DE_PIN, GPIO_PIN_SET )
// Reset DE pin for HD44780 display ===================:
#define HD44780_RESET_DE()		HAL_GPIO_WritePin( HD44780_DE_PORT, HD44780_DE_PIN, GPIO_PIN_RESET )

// Set RS pin for HD44780 display =====================:
#define HD44780_SET_RS()		HAL_GPIO_WritePin( HD44780_RS_PORT, HD44780_RS_PIN, GPIO_PIN_SET )
// Reset RS pin for HD44780 display =====================:
#define HD44780_RESET_RS()		HAL_GPIO_WritePin( HD44780_RS_PORT, HD44780_RS_PIN, GPIO_PIN_RESET )

// --------------------------------------------------------- Global variables --------------------:

// --------------------------------------------------------- Private variables -------------------:
const uint8_t lcd_code[257];	// todo: delete useless symbols.

// --------------------------------------------------------- Private function prototypes ---------:
static void initPins(void);
static void sendInstruction(uint8_t dataCode);
static void sendSymbol(uint8_t symbolCode);
// --------------------------------------------------------- Global functions --------------------:

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @HD44780_Init
  *	&	@brief  Initialization GPIOs for HD44780 Display.
  *				Set display settings.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void HD44780_Init(void)
{
	initPins();

	HAL_Delay(15);
	sendInstruction(0x30);	// 8bits modes setting.

	HAL_Delay(5);
	sendInstruction(0x30);	// 8bits modes setting.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x30);	// 8bits modes setting.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x38);	// 8bits modes setting. 2 strings. 5x8 dots.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x08);	// Display turn OFF.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x01);	// Clear display.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x06);	// Increment AC ; without shift.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x80); 	// AC = 0x00.

	HAL_Delay(1);				// delay > 300 us.
	sendInstruction(0x0C);	// Display turn ON without markers.

	return;
}

/***********************@HD44780_displayString
 *		@brief  Send string to HD44780 LCD to be displayed.
 *  	@param  address of string.
 *  	@retval None
 **********************/
void HD44780_displayString(char *string)		// executing time about 4us.
{
	static uint8_t symbolCnt = 0;
	for(uint8_t j=0; j<16; j++)		__NOP();
	if(string[symbolCnt] != '\0')
		sendSymbol(lcd_code[string[symbolCnt++]]);
	else
	{
		symbolCnt = 0;
		sendInstruction(0x80); 	// AC = 0x00.
	}
}

// --------------------------------------------------------- Private functions -------------------:

/***********************@initPins
 *		@brief  Initialize pins for Data, RS, DE HD44780.
 *  	@param  symbolCode.
 *  	@retval None
 **********************/
static void initPins(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

	/*Configure GPIO pins : HD44780 Data D0..D7 */
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

	GPIO_InitStruct.Pin = HD44780_D0_PIN;
	HAL_GPIO_Init(HD44780_D0_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D1_PIN;
	HAL_GPIO_Init(HD44780_D1_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D2_PIN;
	HAL_GPIO_Init(HD44780_D2_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D3_PIN;
	HAL_GPIO_Init(HD44780_D3_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D4_PIN;
	HAL_GPIO_Init(HD44780_D4_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D5_PIN;
	HAL_GPIO_Init(HD44780_D5_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D6_PIN;
	HAL_GPIO_Init(HD44780_D6_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_D7_PIN;
	HAL_GPIO_Init(HD44780_D7_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : HD44780 RS, DE */
	GPIO_InitStruct.Pin = HD44780_DE_PIN;
	HAL_GPIO_Init(HD44780_DE_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = HD44780_RS_PIN;
	HAL_GPIO_Init(HD44780_RS_PORT, &GPIO_InitStruct);
}


/***********************@sendInstruction
 *		@brief  Send instruction to HD44780 LCD
 *  	@param  symbolCode.
 *  	@retval None
 **********************/
static void sendInstruction(uint8_t instructionCode)
{
	HD44780_SET_DATA(instructionCode);
	HD44780_RESET_RS();
	HD44780_SET_DE();
	for(uint8_t j=0; j<16; j++)		__NOP();
	HD44780_RESET_DE();
	return;
}


/***********************@sendSymbol
 *		@brief  Send symbol to HD44780 LCD
 *  	@param  symbolCode.
 *  	@retval None
 **********************/
static void sendSymbol(uint8_t symbolCode)		// executing time about 2us.
{
	HD44780_SET_DATA(symbolCode);
	HD44780_SET_RS();
	HD44780_SET_DE();
	for(uint8_t j=0; j<16; j++)		__NOP();
	HD44780_RESET_DE();
	return;
}






//void LCD_goto_XY(uint8_t X_position, uint8_t Y_position)	// ������� �������� ��������� AC � �����
//															// ������ � ������������ XY
//{
//	uint8_t position = 0;
//	switch(Y_position)
//	{
//		case 0:
//		position = X_position;
//		break;
//
//		case 1:
//		position = 0x40 + X_position;
//		break;
//
//		case 2:
//		position = 0x14 + X_position;
//		break;
//
//		case 3:
//		position = 0x54 + X_position;
//		break;
//
//		default:
//		break;
//	}
//	position|= 0x80;
//	sendInstruction(position);
//}
//
//void LCD_display_ROM_string_XY(const unsigned char *string, uint8_t X_position, uint8_t Y_position)
//					// ������� ������� �� LCD ������, ����������� �� ����-������
//					// �� ������ string, ������� � ������� XY
//{
//	uint8_t i =0;	// ����� ������� � ������
//
//	LCD_goto_XY(X_position, Y_position);
//	while(string[i] != '\0')	// ���� ������ �� ����������
//	{
//		sendSymbol(lcd_code[string[i++]]);
//	}
//}
//
//void LCD_display_string_XY(unsigned char *simple_string, uint8_t X_position, uint8_t Y_position)
//					// ������� ������� �� LCD ������ simple_string,
//					// ������� � ������� XY
//{
//	LCD_goto_XY(X_position, Y_position);
//	while(*simple_string != '\0')	// ���� ������ �� ����������
//	{
//		sendSymbol(lcd_code[*(simple_string++)]);
//	}
//}
//
//void LCD_display_symbol_XY(unsigned char smbl,uint8_t X_position, uint8_t Y_position)
//					// ������� ������� �� LCD ������ smbl,
//					// ������� � ������� XY
//{
//	LCD_goto_XY(X_position, Y_position);
//	sendSymbol(lcd_code[smbl]);
//}
//
//void LCD_display_symbol(unsigned char simple_smbl)
//					// ������� ������� �� LCD ������ simple_smbl
//{
//	sendSymbol(lcd_code[simple_smbl]);
//}
//
//void LCD_display_int_XY(int number, uint8_t quantity_of_digits, uint8_t X_position, uint8_t Y_position)
//					// ������� ������� q_of_d �������� ����� number ���� int
//					// �� LCD � ������ �����, ������� � ������� XY
//{
//	unsigned char str_from_number[quantity_of_digits+1];
//	char i=quantity_of_digits+1;
//
//	if (number<0)
//	{
//		str_from_number[0] = '-';
//		number*=-1;
//	}
//	else
//	{
//		str_from_number[0] = ' ';
//	}
//
//	str_from_number[i] = '\0';
//	for(i=quantity_of_digits; i>0; i--)
//	{
//		str_from_number[i] = number%10 + '0';
//		number /= 10;
//	}
//
//	LCD_display_string_XY(str_from_number, X_position, Y_position);
//}
//
//void LCD_clear(void)		// ������� ������� �������
//{
//	sendInstruction(0x01);	// ������� �������.
//	_delay_ms(10);			// !!!! �������� ��� �����. �� ������!?!?!!
//}
//
//void LCD_turn_ON(uint8_t pointer_swtch, uint8_t blink_swtch)
//					// ������� ��������� ����������� �� LCD
//					// � ������������ ����������� ��������� �/��� ��������� ���������
//{
//	sendInstruction(0x0C|(pointer_swtch<<1)|blink_swtch);
//}

// ################################################# Exported constants ###########################:
// Symbol codes for HD44780:
const uint8_t lcd_code[257] =
{
//0x00,	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//1

//0x0a,	0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//2

//0x14, 0x15, 0x16, 0x17,  up,  down, 0x1a, 0x1b, 0x1c, 0x1d,
  0x20, 0x20, 0x20, 0x20, 0xd9, 0xda, 0x20, 0x20, 0x20, 0x20,//3

//0x1e, 0x1f, 0x20   !     "     #     $     %     &     '
  0x20, 0x20, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,//4

// (     )     *     +	   ,     -     .     /	   0     1
  0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31,//5

// 2     3     4     5     6     7     8     9     :     ;
  0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b,//6

// <     =     >     ?	   @	 A     B     C	   D     E
  0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45,//7

// F     G     H     I     J	 K     L     M     N     O
  0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,//8

// P     Q     R     S     T     U     V     W     X     Y
  0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,//9

// Z     [    0x5c   ]	   ^     _     `     a	   b     c
  0x5a, 0x5b, 0x20, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63,//10

// d     e     f     g     h     i     j     k     l     m
  0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d,//11

// n     o     p     q     r     s     t     u	   v     w
  0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,//12

// x     y     z    0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81,
  0x78, 0x79, 0x7a, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//13

//0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//14

//0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//15

//0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,//16

//0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7   �	0xa9,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xa2, 0x20,//17

//0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,   �   0xb1, 0xb2, 0xb3,
  0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0xef, 0x20, 0x20, 0x20,//18

//0xb4, 0xb5, 0xb6, 0xb7   �	0xb9, 0xba, 0xbb, 0xbc, 0xbd,
  0x20, 0x20, 0x20, 0x20, 0xb5, 0x20, 0x20, 0x20, 0x20, 0x20,//19

//0xbe, 0xbf,  �     �	   �	 �     �     �     �	 �
  0x20, 0x20, 0x41, 0xa0, 0x42, 0xa1, 0xe0, 0x45, 0xa3, 0xa4,//20

// �     �     �     �	   �	 �     �     �	   �	 �
  0xa5, 0xa6, 0x4b, 0xa7, 0x4d, 0x48, 0x4f, 0xa8, 0x50, 0x43,//21

// �	 �     �     �     �     �     �     �     �	 �
  0x54, 0xa9, 0xaa, 0x58, 0xe1, 0xab, 0xac, 0xe2, 0xad, 0xae,//22

// �	 �     �     �	   �	 �     �     �	   �	 �
  0x62, 0xaf, 0xb0, 0xb1, 0x61, 0xb2, 0xb3, 0xb4, 0xe3, 0x65,//23

// �	 �     �     �	   �	 �     �     �	   �	 �
  0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0x6f, 0xbe,//24

// �	 �     �     �	   �	 �     �     �	   �	 �
  0x70, 0x63, 0xbf, 0x79, 0xe4, 0x78, 0xe5, 0xc0, 0xc1, 0xe6,//25

// �	 �     �     �     � 	 �
  0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7
};


/**********************************************************************************END OF FILE****/
