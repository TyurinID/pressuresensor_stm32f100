/**
  ************************************************************************************************
  * @file	 HD44780.h
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    19-December-2016
  * @brief   Header for HD44780 Display.
  * @note	 In 2-line mode
  * 			1st line is in 0x00 to 0x27 in DDRAM and
  * 			2nd line is in 0x40 to 0x67.
  *************************************************************************************************
  */
#ifndef __HD44780
#define __HD44780

// -------------------------------------------------------- Includes -----------------------------:
#include "main.h"

// -------------------------------------------------------- Exported define ----------------------:
// HD44780 DDRAM lines addresses:
#define DDRAM_1LINE_START		0x00
#define DDRAM_2LINE_START		0x40

// Maximum length of displaying string
#define DISPLAING_STRING_SIZE		25  // in chars.

// Define GPIOs connected to HD44780 Data pins =================:
#define	HD44780_D0_PORT			GPIOA
#define	HD44780_D0_PIN			GPIO_PIN_8
#define	HD44780_D1_PORT			GPIOA
#define	HD44780_D1_PIN			GPIO_PIN_9
#define	HD44780_D2_PORT			GPIOA
#define	HD44780_D2_PIN			GPIO_PIN_10
#define	HD44780_D3_PORT			GPIOA
#define	HD44780_D3_PIN			GPIO_PIN_11
#define	HD44780_D4_PORT			GPIOA
#define	HD44780_D4_PIN			GPIO_PIN_12
#define	HD44780_D5_PORT			GPIOC
#define	HD44780_D5_PIN			GPIO_PIN_10
#define	HD44780_D6_PORT			GPIOC
#define	HD44780_D6_PIN			GPIO_PIN_11
#define	HD44780_D7_PORT			GPIOC
#define	HD44780_D7_PIN			GPIO_PIN_12

// Define GPIOs connected to HD44780 Control pins ==============:
#define	HD44780_DE_PORT			GPIOC
#define	HD44780_DE_PIN			GPIO_PIN_7
#define	HD44780_RS_PORT			GPIOC
#define	HD44780_RS_PIN			GPIO_PIN_6

// -------------------------------------------------------- Exported types -----------------------:


// -------------------------------------------------------- Private macros -----------------------:

// -------------------------------------------------------- Exported variables -------------------:
extern char gDisplayingString[DISPLAING_STRING_SIZE];


// -------------------------------------------------------- Exported functions -------------------:
extern void HD44780_Init(void);  // Initialization GPIOs for HD44780 Display.
extern void HD44780_displayString(char *string);

// -------------------------------------------------------- Exported constants -------------------:


#endif /* __HD44780 */

/**********************************************************************************END OF FILE****/
