/**
  *************************************************************************************************
  * @file	 PressureSensor.h
  * @author  Tyurin Ivan
  * @version V1.2
  * @date    25-December-2016
  * @brief   Calculations of pressure.
  *
  *************************************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PRESSURE_SENSOR_H
#define __PRESSURE_SENSOR_H


#define PRESSURE_BUFF_SIZE		256
#define AUTOZERO_TIMEOUT		3000		// in ms.

extern uint16_t	gPressureBuff[PRESSURE_BUFF_SIZE];		// Buffer for ADC pressure data transfered by DMA.
extern uint8_t  	gPressureBuffRefilled;					// FLG "all data in PressureBuff overwritten".

extern void PressSens_Init(void);
extern void PressSens_Autozero(void);
extern uint16_t PressSens_Calculate(void);

#endif /* __PRESSURE_SENSOR_H */

