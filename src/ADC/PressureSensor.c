/**
  *************************************************************************************************
  * @file	 PressureSensor.c
  * @author  Tyurin Ivan
  * @version V1.2
  * @date    25-December-2016
  * @brief   Calculations of pressure.
  *
  *************************************************************************************************
  */

#include "main.h"
#include "ADC/PressureSensor.h"
#include "ADC/adc.h"

uint16_t	gPressureBuff[PRESSURE_BUFF_SIZE];		// Buffer for ADC pressure data transfered by DMA.
uint8_t  	gPressureBuffRefilled;					// FLG "all data in PressureBuff overwritten".
static uint16_t	gPressureZeroCode;						// Correction zero pressure value in code_adc.


/***********************@PressSens_Init
 *		@brief  Initialization of PressureSensor parameters.
 *  	@param  None
 *  	@retval None
 **********************/
void PressSens_Init(void)
{
	gPressureBuffRefilled = 0;
	gPressureZeroCode     = 0;
	for(uint16_t i=0; i<PRESSURE_BUFF_SIZE; i++)
		gPressureBuff[i] = 0;
}

/***********************@PressSens_Autozero
 *		@brief  Remember ADC code for zero pressure.
 *  	@param  None
 *  	@retval None
 **********************/
void PressSens_Autozero(void)
{
	uint16_t time = 0;
	// Skip first 8 cycles:
	for(uint8_t i=0; i<8; i++)
	{
		while(gPressureBuffRefilled != 1)				// Wait for first Pressure Buffer filling.
		{
			HAL_Delay(1);
			if(++time > AUTOZERO_TIMEOUT)				// If there are no signals from ADC for long time, return ERR.
			{
				gError = AUTOZERO_TIMEOUT_ERR;
				return;
			}
		}
	}
	while(gPressureBuffRefilled != 1)				// Wait for first Pressure Buffer filling.
	{
		HAL_Delay(1);
		if(++time > AUTOZERO_TIMEOUT)				// If there are no signals from ADC for long time, return ERR.
		{
			gError = AUTOZERO_TIMEOUT_ERR;
			return;
		}
	}

	uint32_t temp_sum = 0;
	for(uint16_t i=0; i<PRESSURE_BUFF_SIZE; i++)
		temp_sum += (uint8_t)(gPressureBuff[i]>>8);
	gPressureZeroCode = temp_sum/PRESSURE_BUFF_SIZE;
	__NOP();
}

/***********************@PressSens_Calculate
 *		@brief  Calculate pressure value from ADC buffer
 *				according to zero pressure code.
 *  	@param  None
 *  	@retval Calculated pressure value in Pascal.
 **********************/
uint16_t PressSens_Calculate()
{
	uint32_t temp_sum = 0;
	for(uint16_t i=0; i<PRESSURE_BUFF_SIZE; i++)
		temp_sum += (uint8_t)(gPressureBuff[i]>>8);

	temp_sum = (uint16_t)((temp_sum/PRESSURE_BUFF_SIZE-gPressureZeroCode) * 11.8f); // in mv

	return ( ((float)temp_sum/(5000.0f))/0.009f * 1000 );		 // code_adc * 11.5 = mV.
//	return ( (uint16_t)((temp_sum/PRESSURE_BUFF_SIZE-gPressureZeroCode) * 11.8f) );		 // code_adc * 11.5 = mV.
	//	return ( (uint16_t)((temp_sum-gPressureZeroCode) * (uint16_t)(5000/256)) );		 // code_adc 256 = 5V = 5000 Pa.
}
