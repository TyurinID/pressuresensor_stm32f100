/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ADC_H
#define __ADC_H







// ################################################# Exported variables ###########################:
extern ADC_HandleTypeDef    AdcHandle;
extern TIM_HandleTypeDef    TimHandle;

// ################################################# Exported functions ###########################:
extern void ADC_Config(void);
extern void TIM_Config(void);




#endif /* __ADC_H */

/**********************************************************************************END OF FILE****/
