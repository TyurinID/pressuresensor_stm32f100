/**
  *************************************************************************************************
  * @file	 adc.c
  * @author  Tyurin Ivan
  * @version V1.1
  * @date    15-December-2016
  * @brief   Contains function for Configuration ADC and associated TIM. Works with HAL library!
  * 		 (++) Connect stm32xxxx_hal_adc.c and stm32xxxx_hal_adc_ex.c  to the project.
  * 		 (++) Connect and configure adc_hal_msp.c file (MCU Support Package) to the project.
  * 		 (++) Configure functions in this file.
  * 		 (++) Insert ADC and TIM interrupt handlers here.
  *************************************************************************************************
  */

// ##################################################### Includes #################################:
#include "main.h"
#include "adc.h"
#include "ADC/PressureSensor.h"

// ################################################# Private typedef ##############################:

// ################################################# Private define ###############################:
// Timer define:
#define TIMER_FREQUENCY                ((uint32_t) 1000)    // Timer frequency (unit: Hz). With a timer 16 bits and time base freq min 1Hz, range is min=1Hz, max=32kHz.
#define TIMER_FREQUENCY_RANGE_MIN      ((uint32_t)    1)    // Timer minimum frequency (unit: Hz). With a timer 16 bits, maximum frequency will be 32000 times this value.
#define TIMER_PRESCALER_MAX_VALUE      (0xFFFF-1)           // Timer prescaler maximum value (0xFFFF for a timer 16 bits)

// ################################################# Private macro ################################:

// ################################################# Global variables #############################:
ADC_HandleTypeDef  	 AdcHandle;
TIM_HandleTypeDef    TimHandle;

// ################################################# Private variables ############################:

// ############################################ Private function prototypes #######################:
void SystemClock_Config(void);

// ################################################# Private functions ############################:


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ADC_Config
  *	&	@brief  ADC Configuration.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ADC_Config(void)
{
	  // ADC1 configuration ==================================================================:
	  ADC_ChannelConfTypeDef   sConfig;

	  // Configuration of ADC1 init structure: ADC parameters and regular group ===:
	  AdcHandle.Instance = ADC1;

	  AdcHandle.Init.DataAlign             = ADC_DATAALIGN_LEFT;
	  AdcHandle.Init.ScanConvMode          = ADC_SCAN_DISABLE;              // Sequencer disabled (ADC conversion on only 1 channel).
	  AdcHandle.Init.ContinuousConvMode    = DISABLE;                       // Continuous mode disabled to have only 1 conversion at each conversion trig.
//	  AdcHandle.Init.NbrOfConversion       = 1;                             /* Parameter discarded because sequencer is disabled */
//	  AdcHandle.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
//	  AdcHandle.Init.NbrOfDiscConversion   = 1;                             /* Parameter discarded because sequencer is disabled */
	  AdcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T3_TRGO;  // Trig of conversion start done by external event.

	  if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
		  Error_Handler();		// ADC initialization error.


  	  // Configuration of channel on ADC1 regular group ==============================:
	  sConfig.Channel      = ADC_CHANNEL_1;
	  sConfig.Rank         = ADC_REGULAR_RANK_1;
	  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;

	  if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	  		Error_Handler();	// Channel Configuration Error.
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @TIM_Config
  *	&	@brief  Timers Configuration.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void TIM_Config(void)
{
	  // todo: rewrite simpler!

	  TIM_MasterConfigTypeDef master_timer_config;
	  RCC_ClkInitTypeDef clk_init_struct = {0};       // Temporary variable to retrieve RCC clock configuration.
	  uint32_t latency;                               // Temporary variable to retrieve Flash Latency.

	  uint32_t timer_clock_frequency = 0;             // Timer clock frequency.
	  uint32_t timer_prescaler = 0;                   // Time base prescaler to have timebase aligned on minimum frequency possible.

	  /* Configuration of timer as time base:                                     */
	  /* Caution: Computation of frequency is done for a timer instance on APB1   */
	  /*          (clocked by PCLK1)                                              */
	  /* Timer period can be adjusted by modifying the following constants:       */
	  /* - TIMER_FREQUENCY: timer frequency (unit: Hz).                           */
	  /* - TIMER_FREQUENCY_RANGE_MIN: timer minimum frequency (unit: Hz).         */

  //  Get timer clock source frequency:
  HAL_RCC_GetClockConfig(&clk_init_struct, &latency);
  // If APB1 prescaler is different of 1, timers have a factor x2 on their clock source.                                                            */
  if (clk_init_struct.APB1CLKDivider == RCC_HCLK_DIV1)
  {
    timer_clock_frequency = HAL_RCC_GetPCLK1Freq();
  }
  else
  {
    timer_clock_frequency = HAL_RCC_GetPCLK1Freq() *2;
  }

  // Timer prescaler calculation:
  /* (computation for timer 16 bits, additional + 1 to round the prescaler up) */
  timer_prescaler = (timer_clock_frequency / (TIMER_PRESCALER_MAX_VALUE * TIMER_FREQUENCY_RANGE_MIN)) +1;

  /* Set timer instance */
  TimHandle.Instance = TIM3;

  /* Configure timer parameters */
  TimHandle.Init.Period            = ((timer_clock_frequency / (timer_prescaler * TIMER_FREQUENCY)) - 1);
  TimHandle.Init.Prescaler         = (timer_prescaler - 1);
  TimHandle.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
  TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle.Init.RepetitionCounter = 0x0;

  if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
  {
    /* Timer initialization Error */
    Error_Handler();
  }

  /* Timer TRGO selection */
  master_timer_config.MasterOutputTrigger = TIM_TRGO_UPDATE;
  master_timer_config.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;

  if (HAL_TIMEx_MasterConfigSynchronization(&TimHandle, &master_timer_config) != HAL_OK)
  {
    /* Timer TRGO selection Error */
    Error_Handler();
  }
}


/* ############################################################################################## */
/* ####                                 ADC Interrupt Handlers                               #### */
/* ############################################################################################## */


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ADC1_IRQHandler
  *	&	@brief  This function handles ADC1 interrupt request.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void ADC1_IRQHandler(void)
{
	// todo: rewrite simpler.
  HAL_ADC_IRQHandler(&AdcHandle);
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @ADC1_IRQHandler
  *	&	@brief  This function handles ADC1 interrupt request.
  * & 	@param  None
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void DMA1_Channel1_IRQHandler(void)
{
  HAL_DMA_IRQHandler(AdcHandle.DMA_Handle);
}


/* ############################################################################################## */
/* ####                            ADC Interrupt Callbacks                                   #### */
/* ############################################################################################## */


/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @HAL_ADC_ConvCpltCallback
  *	&	@brief  Conversion complete callback in non blocking mode.
  * & 	@param  AdcHandle
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *AdcHandle)
{
	gPressureBuffRefilled = 1;		//
}

/** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& @HAL_ADC_ErrorCallback
  *	&	@brief  ADC error callback in non blocking mode.
  * & 	@param  AdcHandle
  * & 	@retval None
  * &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc)
{
	gError = ADC_DMA_ERR;
}

/**********************************************************************************END OF FILE****/
